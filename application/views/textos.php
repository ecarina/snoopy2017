<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- MECÁNICA -->

<div class="modal fade" id="ModalMecanica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?= base_url('assets/img/close.png');?>"></button>
         </div>
         <div class="modal-body text-center">
            <div class="titleTexto">Mecánica</div>
            <div class="textoMecanica">
               <p>1. Da clic en ¡Participa!</p>
               <p>2. Ingresa con tu cuenta de Facebook</p>
               <p>3. Sube la foto de tu ticket de compra, ingresa el número de ticket y elige la tienda

               </p>
               <p>4. Calcula cuántas cátsup La Costeña® hay en la casita y da clic en "Enviar"</p>
               <p>5. Compártelo en Facebook</p>
               <p>6. Daremos a conocer los 5 ganadores el 13 de octubre de 2017</p>
            </div>
         </div>
      </div>
   </div>
</div>


<!-- PREMIOS -->


<div class="modal fade" id="ModalPremios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?= base_url('assets/img/close.png');?>"></button>
         </div>
         <div class="modal-body text-center">
            <div class="titleTexto">Premios</div>
            <div class="col-md-6 textoPremios">
               <p>Calcula cuantas cátsups hay en la casita de Snoopy® y sé uno de los afortunados ganadores de estos premios:
               </p>
               <p>&nbsp;</p>
               <p>5 kits escolares con laptop, mochila, lapicera, cuaderno Snoopy® y kit de aderezos premium.</p>
            </div>
            <div class="col-md-6 imgPremios"><img src="<?= base_url('assets/img/premios.png'); ?>"></div>
         </div>
      </div>
   </div>
</div>



<!-- BASES -->


<div class="modal fade" id="ModalBases" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?= base_url('assets/img/close.png');?>"></button>
         </div>
         <div class="modal-body text-center">
            <div class="titleTexto">Bases</div>
            <div class="textoBases">  
              <div class="body" style="white-space:pre-wrap; line-break:strict; margin: 0 5px;">
                <p><span>BASES DEL CONCURSO</span></p>
                <p><span>“CÁTSUPS EN LA CASA”</span></p>
                <p><span>Nombre de la Promoción: CÁTSUPS EN LA CASA</span></p>
                <p><span>Modalidad: </span><span>Concurso.</span></p>
                <p><span>Responsable de la Promoción</span><span>: Conservas la Costeña, S.A. de C.V. con domicilio en Vía Morelos 268, colonia Santa María Tulpetlac, Ecatepec, Estado de México, Código Postal 55400, Estados Unidos Mexicanos.</span></p>
                <p><span>El portal web </span><a href="http://www.facebook.com/"><spandir="ltr">www.facebook.com</span></a><span> y sus compañías relacionadas no ostentan ningún tipo de participación, responsabilidad y administración de este Concurso, incluyendo la premiación. </span></p><p><span>Requisito de compra para participar en la Promoción: </span>Comprar $50 en productos La Costeña en cualquier tienda de autoservicio y/o supermercado y conservar el ticket de compra y enviarlo vía inbox de facebook. </p><p><span>Bien objeto de la Promoción</span><span>: </span>Todos los productos comercializados al amparo de la marca La Costeña® </p>
                <p><span>Vigencia de la Promoción: </span><span>El concurso iniciará a l</span>as 0:00 horas del 25 de Agosto de 2017 y terminará a las 23:00 horas del 8 de Octubre del 2017. </p>
                <p><span>Área geográfica: </span><span>Promoción válida a nivel nacional en la República Mexicana. </span></p>
                <p><span>Para preguntas o comentarios sobre esta promoción, comunicarse al correo electrónico </span><a href="mailto:contactomercadotecn@lacostena.com.mx"><spandir="ltr">contactomercadotecn@lacostena.com.mx</span></a><span> o al número telefónico (01 55) 5282-7676 ext. 5460 o 5457 en un horario de 7:30 a 13:00 y de 14:00 a 17:00 horas.</span></p>
                <p><span>TÉRMINOS Y CONDICIONES.</span></p>
                <ol style="padding:0; margin:0;"><li value="1" ><span>Requisitos para concursar.</span></li></ol>
               <p><span>Para poder ser participante de la Promoción, es necesario: (i) ser una persona mayor de 18 años en plena capacidad de goce y ejercicio, (ii) residir en la República Mexicana, (iii) estar debidamente registrado como usuario de Facebook en el Sitio Web </span><a href="http://www.facebook.com/"><spandir="ltr">www.facebook.com</span></a><span> durante todo el tiempo de vigencia de la Promoción, (iv) contar con un perfil verdadero y acreditable en dicha red social, (v) ser seguidores de la “</span><span>Fan Page</span><span>” de La Costeña® en Facebook ubicada en </span><a href="http://www.facebook.com/lacostenamx/"><spandir="ltr">www.facebook.com/lacostenamx/</span></a><span>, (vi) no haber resultado ganadores de ninguna dinámica o concurso organizado a través de </span><a href="http://www.facebook.com/lacostenamx"><spandir="ltr">www.facebook.com/lacostenamx</span></a><span> dentro de los seis (6) meses anteriores a la fecha de terminación de la vigencia de este</span> Concurso y, (vii) realizar la compra de $50 en productos La Costeña en cualquier tienda de autoservicio y/o supermercado y conservar el ticket de compra y enviarlo vía inbox de facebook. </p>
               <p>No podrán participar en el concurso las personas que hayan intervenido en la organización del mismo, ni los empleados o familiares de primer grado de los organizadores, de cualquiera de sus filiales, de cualquier sociedad que tenga participación en las sociedades respectivas de los organizadores o de cualquier sociedad vinculada a los organizadores, ni sus familiares, ascendientes, descendientes, cónyuges o parejas de hecho.</p>
               <p>No podrán participar en el concurso personal dependiente de Conservas La Costeña, S.A. de C.V. ni de Havas Media Services, S.A. de C.V., ni de sus sociedades relacionadas, cónyuges, parejas de hecho, sus familiares directos (padres, hijos y hermanos), trabajadores e inquilinos.</p>
               <p>El incumplimiento por cualquier usuario de las condiciones establecidas en las Bases supondrá la anulación automática de dicha participación. Del mismo modo, cualquier declaración falsa o indicios de identidad o domicilio falso ocasionará la descalificación inmediata del usuario y, en su caso, la obligación de devolver el Premio si ya le hubiese sido entregado, así como el intento de hackear o realizar alguna acción tendiente a modificar o eliminar indebidamente a su favor el contenido o los mecanismos de seguridad de las páginas o sitios web involucrados en la Promoción.</p>
               <p><span>La aceptación de estas Bases es condición necesaria para la participación en el Concurso y, en consecuencia, para poder obtener el Premio. Por el simple hecho de su participación en el concurso, el participante acepta y reconoce que ha leído, entendido y acepta las presentes Bases. </span></p>
               <ol style="padding:0; margin:0;"><li value="2"><span>Mecánica.</span></li></ol>
               <p>El concurso consiste en calcular la cantidad total de botellas de Salsa Cátsup marca La Costeña® en su edición de Snoopy® que contiene una casa de perro hecha de acrílico transparente. Resultarán ganadoras las primeras cinco personas que calculen la cantidad exacta de botellas que contiene la casa, o bien, las que más se aproximen a esta cantidad.</p>
               <p><span><bdi>a)</bdi></span>Para participar en la promoción, los participantes deberán:</p>
               <p><span><bdi>(i)</bdi></span>Seguir a La Costeña® en Facebook, dando clic en el apartado correspondiente en la “Fan Page” de la Costeña® disponible en la dirección electrónica <a href="http://www.facebook.com/lacostenamx/"><spandir="ltr">www.facebook.com/lacostenamx/</span></a></p>
               <p><span><bdi>(ii)</bdi></span>Comprar $50 en productos La Costeña en cualquier tienda de autoservicio y/o supermercado y conservar el ticket de compra y enviarlo vía inbox de facebook.</p>
               <p><span><bdi>(iii)</bdi></span>Ingresar al sitio web<a href="http://www.lacostena.com.mx/ponlelodivertido"><spandir="ltr">www.</span><spandir="ltr">lacostena.com.mx/ponlelodivertido</span></a><span class="c11"> y llevar a cabo el siguiente procedimiento: </span></p>
               <p><span>Paso 1.</span> Registrarse en el sitio web a través de su cuenta de Facebook. </p>
               <p><span>Paso 2.</span> Subir una fotografía clara y perfectamente legible del ticket de compra con productos La Costeña, registrando además el número de ticket correspondiente y seleccionar la tienda de autoservicio o supermercado donde fue adquirida.</p>
               <p><span>Paso 3. </span>Observar la fotografía en 360º publicada en el sitio web, para calcular la cantidad de botellas de Salsa Cátsup que contiene la casita de perro y registrar el resultado de su cálculo en el sitio web.</p>
               <p><span>Sólo se aceptará una participación por cada Ticket de compra, inclusive si dicho Ticket contiene la compra de más de $50 M/N, es decir, una persona puede participar las veces que quiera siempre y cuando registre un Ticket de compra diferente en cada participación. Para que el Ticket sea válido para participar en el concurso, es necesario que la compra de </span>productos La Costeña<span> se haya realizado dentro del periodo de vigencia de la promoción. No serán válidos para efecto de participar en el concurso, los tickets que hayan sido emitidos antes o después de la vigencia de promoción.</span></p>
               <p><span><bdi>b)</bdi></span>Los participantes tendrán a partir de las 0:00 horas del 25 de Agosto de 2017 hasta las 23:00 horas del 8 de octubre de 2017, para poder participar. </p>
               <p><span><bdi>c)</bdi></span>Una vez transcurrido el plazo antes mencionado, el día 11 de octubre de 2017 a las 12:00 horas se llevará a cabo una transmisión por Facebook Live en la fan page de La Costeña®, en la cual se llevará a cabo la lectura de fe de hechos, previamente realizada ante el notario público número 22 donde se enuncia el total de botellas.</p>
               <p><span><bdi>d)</bdi></span>El viernes 13 de octubre de 2017, se publicarán en la “Fan Page” de La Costeña® (<a href="http://www.facebook.com/lacostenamx"><spandir="ltr">www.facebook.com/lacostenamx</span></a>) los 5 participantes ganadores.</p>
               <ol style="padding:0; margin:0;"><li value="3"><span>Ganadores y Premio.</span></li></ol>
               <p><span>Serán ganadores los 5 primeros participantes que hayan acertado o que más se hayan aproximado al total de botellas de Salsa Cátsup contenidas dentro de la casita de perro. </span></p>
               <p><span>Para efecto de determinar los ganadores, se tomará en cuenta el día y hora de su participación, de modo tal que si existen más de cinco personas que acierten la cantidad exacta de botellas, los ganadores serán los primeros que hayan registrado su participación con la cantidad exacta. </span></p>
               <p><span>Por otra parte, si no se adivina la cantidad exacta por todos o alguno(s) de los ganadores, resultarán ganadores quienes se aproximen más al número exacto, independientemente de si éste fuere mayor o menor a la cantidad exacta de botellas (por ejemplo, si la cantidad exacta de botellas en la casa fuera de 50 y hubiere dos participantes, uno que registró 48 botellas y uno que registró 51 botellas, resultará ganador quien hubiere registrado 51 botellas, aún si esta cantidad es mayor a la exacta). </span></p>
               <p><span>Cada uno de los ganadores recibirá como premio un Kit escolar que incluye:</span></p>
               <ul style="padding:0 0 0 1px; margin:0;"><li><span>Mochila Chenson de Snoopy (1)</span></li></ul>
               <ul style="padding:0 0 0 1px; margin:0;"><li><span>Lapicera Chenson de Snoopy (1)</span></li></ul>
               <ul style="padding:0 0 0 1px; margin:0;"><li><span>Libreta Novelty Snoopy (1)</span></li></ul>
               <ul style="padding:0 0 0 1px; margin:0;"><li><span>Computadora Laptop marca HP modelo 15-AY008LA (1)</span></li></ul>
               <p><span>El premio incluye los gastos de envío (por una sola vez) del premio por servicio de mensajería especializada al domicilio del ganador, si éste viviera fuera de la Ciudad de México o Área Metropolitana.</span></p>
               <p><span>La obtención del Premio está sujeta a las restricciones establecidas en estas Bases, a su disponibilidad y a la legislación aplicable. En caso de que no hubiera disponibilidad de alguno de los elementos del premio, se enviará un bien equivalente con características y precio similares. El premio será personal e intransferible y no será canjeable por dinero en efectivo ni por ningún otro incentivo. La determinación de la obligación del pago de cualquier tipo de impuestos, comisiones, costos de mensajería distintos de los establecidos en estas Bases o tarifas generadas por la redención del Premio, son de absoluta y total responsabilidad de los Participantes ganadores que los hayan recibido.</span></p>
               <p><span>Para que el o los Participantes ganadores residentes en la Ciudad de México y Área Metropolitana  puedan reclamar el Premio que les corresponde, tendrán que presentarse en el domicilio indicado más adelante en estas Bases, firmar la carta de aceptación del premio y presentar el original y copia de su identificación oficial, así como copia impresa de la publicación que le declara ganador y el Ticket </span>de compra de $50 M/N de productos<span> La Costeña® con el que registró su participación.</span></p>
               <p><span>Para que el o los Participantes ganadores residentes fuera de la Ciudad de México o Área Metropolitana puedan reclamar el Premio que les corresponde, tendrán que enviar firmada y escaneada a la dirección de correo electrónico indicada por el responsable de la promoción, la carta de aceptación del premio, la copia de su identificación oficial, el Ticket </span>de compra de $50 M/N de productos<span> La Costeña® con el que registraron su participación, así como un comprobante de domicilio con todos los datos de envío contemplados para poder hacerles llegar el Premio.</span></p>
               <p><span>De igual modo, el Participante ganador se obliga a celebrar cualesquiera documentos que Conservas La Costeña, S.A. de C.V. considere necesarios y/o convenientes para acreditar la recepción del premio a entera satisfacción de Conservas La Costeña, S.A. de C.V., así como para permitir el libre uso y la explotación comercial y no comercial de su nombre e imagen para efecto de anunciar su participación y obtención del premio otorgado por parte de Conservas La Costeña, S.A. de C.V.</span></p>
               <p><span>Conservas La Costeña, S.A. de C.V. se reserva el derecho de modificar las condiciones de obtención del premio, así como la mecánica de este concurso, previa publicación en el “Fan Page” de La Costeña: </span><a href="http://www.facebook.com/lacostenamx/"><spandir="ltr">www.facebook.com/lacostenamx/</span></a></p>
               <ol style="padding:0; margin:0;"><li value="4" ><span>Cantidad de botellas en la casita de perro.</span></li></ol>
               <p>Para efecto de dar fe de la cantidad de botellas que fueron introducidas al interior de la Casita de perro, se comunica a los participantes que con fecha 8 de agosto del 2017, el licenciado Carlos Otero, Notario Público 22 del Estado de México, llevó a cabo una fe de hechos en la cual llevó a cabo personalmente el conteo de botellas que se iban introduciendo en la estructura e hizo constar la cantidad total de estas botellas en el Acta de fe de hechos correspondiente.</p>
               <p>De igual modo, se dio fe de que la casita de perro fue embalada tras haber sido llenada, así como que las botellas fueron introducidas manualmente una por una de manera espontánea, sin ningún tipo de mecanismo o estrategia particular, dejando de introducirse botellas a la estructura cuando a simple vista se apreciaba la casita como totalmente llena. Al Acta fueron anexadas las dimensiones de las botellas introducidas, las dimensiones de la casita de perro y la evidencia fotográfica correspondiente.</p>
               <p><span>Para efecto de determinar los ganadores del concurso, únicamente se tomará en cuenta la cantidad total de botellas consignada en el Acta de fe de hechos mencionada, misma que será revelada durante la transmisión en Facebook Live a que hacen referencia las presentes bases, </span>el día 11 de octubre de 2017,<span> sin que ningún otro acto, cálculo, diligencia o conteo resulte válido para efecto de determinar los ganadores.</span></p>
               <ol style="padding:0; margin:0;"><li value="5" ><span>Restricciones.</span></li></ol>
               <p><span><bdi>a)</bdi></span>La participación está limitada a una oportunidad de calcular la cantidad de botellas de Salsa Cátsup La Costeña® edición de Snoopy® dentro de la casita de perro, por cada Ticket de compra de $50 M/N de productos La Costeña.</p>
               <p><span><bdi>b)</bdi></span>En todo momento será facultad exclusiva e inapelable del responsable de la Promoción, la eliminación de un Participante por cualquiera de las siguientes causales:</p>
               <ol style="padding:0 0 0 50px; margin:0;"><li value="1"><span>Cuando el participante falsifique el ticket de compra.</span></li></ol>
               <ol style="padding:0 0 0 50px; margin:0;"><li value="2"><span>Cuando un participante sea identificado bajo un perfil apócrifo y/o que se detecte que realiza actividades fraudulentas. </span></li></ol>
               <ol style="padding:0 0 0 50px; margin:0;"><li value="3"><span>Cuando un participante no reúna los requisitos de participación establecidos en las presentes Bases.</span></li></ol>
               <ol style="padding:0 0 0 50px; margin:0;"><li value="4"><span>En caso de que se detecte que un perfil en facebook.com o a una persona que tiene como actividad principal la obtención de premios para diferentes marcas (cazapremios). </span></li></ol>
               <ol style="padding:0 0 0 50px; margin:0;"><li value="5"><span>En caso de que se detecte que el participante ha revelado públicamente los cálculos y/o el procedimiento empleados para determinar la cantidad total de botellas en la casita, o bien, que los ha revelado a personas cercanas a su círculo social y/o a familiares, resultando ganadoras todas estas personas. </span></li></ol>
               <ol style="padding:0 0 0 50px; margin:0;"><li value="6"><span>No podrán participar personas que hayan resultado ganadoras en dinámicas anteriores realizadas durante los seis (6) meses anteriores a la fecha de terminación de la vigencia de la presente promoción. </span></li></ol>
               <ol style="padding:0; margin:0;"><li value="6" ><span>Procedimiento para reclamar el Premio.</span></li></ol>
               <p><span>Los participantes ganadores, serán notificados mediante una publicación a través del “</span><span>Fan Page</span><span>” de La Costeña® en Facebook ubicada en </span><a href="http://www.facebook.com/"><spandir="ltr">www.facebook.com</span></a><span>/lacostenamx/, el </span><span>día 13 de Octubre de 2017.</span></p>
               <p><span>A partir de dicha publicación, el participante tendrá cuarenta y ocho (48) horas para contactar a Conservas La Costeña, S.A. de C.V. por el medio que le sea indicado, para reclamar su premio por escrito, enviando su nombre completo y copia de su identificación oficial. </span></p>
               <p><span>Es importante hacer de su conocimiento que si el participante ganador hubiera incluido erróneamente sus datos incluyendo su correo electrónico o su perfil de Facebook, ya sea con errores ortográficos o de dedo, no será responsabilidad del organizador quien, en caso de que no pueda contactar al participante ganador, estará libremente facultado para elegir otro ganador conforme a las presentes Bases.</span></p>
               <p><span>Si llegarán a transcurrir las cuarenta y ocho (48) horas y el premio no hubiera sido reclamado por alguno de los ganadores, se entenderá que el ganador renuncia a su derecho, sin que ésta situación implique remuneración alguna y que, por ello, autoriza a los organizadores a disponer de este premio en la forma que mejor convenga a sus intereses. </span></p>
               <p><span>De no existir un ganador que cumpla con todas las condiciones mencionadas, se podrá seleccionar al siguiente finalista en la lista.</span></p>
               <ol style="padding:0; margin:0;"><li value="7" ><span>Entrega del Premio. </span></li></ol>
               <p><span>Sólo podrán reclamar el premio aquellos participantes ganadores, que hayan enviado o presentado sus documentos dentro de los tiempos descritos. </span></p>
               <p><span>Para recibir su premio, los ganadores que tengan su domicilio dentro de la Ciudad de México y área Metropolitana deberán presentarse personalmente con una identificación oficial con fotografía, el ticket de compra original </span>con $50 M/N de productos La Costeña <span>con que participaron en el concurso y una impresión del correo electrónico y publicación de Facebook que lo declara ganador en el domicilio ubicado en Lago Zurich 245, Edificio Zurich, 8º piso, Colonia Ampliación Granada, Código Postal 11590, Ciudad de México, México, el día: 13 de octubre de 2017 en un horario de 11:00 horas. </span></p>
               <p><span>Para aquellos ganadores que tengan su domicilio fuera de la Ciudad de México y Área Metropolitana deberán haber enviado en su totalidad la documentación requerida a los medios de contacto que se le hayan proporcionado. El envío de su premio (que se realizará por una sola vez) no superará un lapso mayor a 30 días hábiles en el domicilio que ellos mismos refieran.</span></p>
               <ol style="padding:0; margin:0;"><li value="8" ><span>Derechos de Autor y Propiedad Industrial. </span></li></ol>
               <p>El responsable de la Promoción podrá tomar las medidas necesarias para no ser etiquetado y/o relacionado de manera alguna con cualquier contenido o información que sea subido o publicado en las cuentas de redes sociales de los participantes, si así lo considera necesario y/ conveniente, particularmente si alguno de estos participantes infringe los Términos y Condiciones aquí establecidos, sin necesidad de previo aviso o posterior notificación.</p>
               <p>El participante reconoce que la marca La Costeña®, los envases, etiquetas, otras marcas, colores, elementos de imagen e información de los productos de La Costeña®, así como el personaje Snoopy® y los personajes, diseños y obras de arte relacionados con el mismo, se encuentran protegidos como derechos de propiedad intelectual e industrial por sus respectivos titulares y sobre los cuales los organizadores de la promoción tienen derecho y/o licencia para su explotación. El solo hecho de participar o de cualquier otra forma interactuar en relación con la promoción, no otorga al participante y/o persona relacionada ningún derecho de uso, licencia o autorización sobre dichos derechos de Propiedad Industrial e Intelectual. Se incluye, sin limitar, dentro de esta restricción, la prohibición de realizar cualquier copia, reproducción, modificación, alteración, registro o almacenaje por medios electrónicos, descarga (download), divulgación, transmisión, transferencia, promoción, distribución o comercialización de estos derechos. </p>
               <p>Cualquier violación a lo dispuesto en este apartado, hace responsable al participante y/o usuario de las redes sociales o persona relacionada de las sanciones e indemnizaciones que se establezcan en la legislación y normatividad aplicable, las cuales incluirán el reembolso de honorarios de abogados, gastos y costas judiciales.</p>
               <p>Conservas La Costeña, S.A. de C.V. no se hace responsable, en ningún caso, de la publicación y puesta a disposición de las fotografías, videos y de los contenidos subidos por los participantes. Asimismo, excluye cualquier responsabilidad por los daños y perjuicios de toda naturaleza que se pudieran ocasionar, incluyendo los ocasionados por violaciones a derechos de autor y/o derechos de imagen.</p>
               <p><span>El participante se compromete a sacar en paz y a salvo a Conservas La Costeña, S.A. de C.V., sus filiales y asociadas, por cualquier acción legal o demanda que derive del contenido que haya sido subido a las redes sociales en relación con el concurso (incluido, sin limitación, el pago de daños y perjuicios, costas y derechos gubernamentales).</span></p>
               <ol style="padding:0; margin:0;"><li value="9" ><span>Aceptación de las Bases.</span></li></ol>
               <p><span>Por el simple hecho de participar en el concurso, las personas han conocido y aceptado íntegramente estas bases, careciendo del derecho a deducir reclamo o acción de cualquier naturaleza en contra de Conservas La Costeña, S.A. de C.V., así como otorgando su consentimiento para la difusión de su imagen, nombre, fotografía de perfil y perfil de redes sociales con motivo de la presente promoción.</span></p>
               <p><span>Se deja constancia que las presentes bases, son las únicas que regulan la Promoción de manera tal que cualquier otro documento o material queda sin efecto.</span></p>
               <ol style="padding:0; margin:0;"><li value="10"><span>Privacidad de datos personales.</span></li></ol>
               <p><span>Conservas La Costeña, S.A. de C.V. será el responsable del tratamiento de los datos personales que se recaben en la ejecución de la Promoción, mismos que serán utilizados exclusivamente para las siguientes finalidades primarias: (i) para la identificación de los participantes, (ii) para llevar a cabo la promoción aquí descrita; (iii) para llevar a cabo la entrega del premio al ganador. El Aviso de Privacidad completo está a disposición de los participantes en </span><a href="http://www.lacostena.com.mx/"><spandir="ltr">http://www.lacostena.com.mx/</span></a></p>
               <ol style="padding:0; margin:0;"><li value="11"><span>Jurisdicción.</span></li></ol>
               <p><span>En caso de controversia, reclamación o disputa que pueda surgir, sobre la interpretación o cumplimiento de estas Bases, los participantes renuncian expresamente al fuero que pudiera corresponderles, sometiéndose, también expresamente, a la legislación vigente, jurisdicción y competencia de los Juzgados y Tribunales de la Ciudad de México.</span></p>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

