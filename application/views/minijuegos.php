<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!--div class="nube_minigame"><img src="<?= base_url('assets/img/nubes_minigames.png');?>"></div-->
<div class="text-center texto_minigame">Elige el juego que más te guste</div>
<div class="minigame text-center">
      <div class="col-md-4 col-xs-12 mini"><a href="<?= base_url('snoopy/candy');?>" style="border: none;"><img src="<?= base_url('assets/img/candy.png');?>"></a></div>
      <div class="col-md-4 col-xs-12 mini"><a href="<?= base_url('snoopy/personajes');?>" style="border: none;"><img src="<?= base_url('assets/img/personajes.png');?>"></a></div>
      <div class="col-md-4 col-xs-12 mini"><a href="<?= base_url('snoopy/snake');?>" style="border: none;"><img src="<?= base_url('assets/img/snake.png');?>"></a></div>
</div>
<div class="pasto_minigame"><img src="<?= base_url('assets/img/snoopy_bandera.png');?>"></div>
