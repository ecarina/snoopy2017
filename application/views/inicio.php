<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
   <div class="row">
      <div class="col-md-7 txtRed">
         <div class="col-md-12 sinpd">

            <div class="col-md-12 col-xs-12 text-center sinpd">
               <div class="col-md-10 sinpd">
                  <iframe src="https://www.youtube.com/embed/z3JAKwuTisE?rel=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
               </div>
               <div class="col-md-2"></div>
            </div>

            <div class="col-md-12 sinpd">
               <div class="col-md-10 col-xs-9 miniText sinpd">
                  <p class="txtBig text-center">¡Ponle lo divertido!<span class="superindice">®</span><br>con La Costeña<span class="superindice">®</span></p>
                  <hr>

                  <div class="txtSmall text-center">Compra $50 pesos en productos La Costeña®, registra tu ticket ¡Y gana uno de los 5 kits back to school!</div>
               </div>
               <div class="col-md-2 col-xs-3 snoopyPasto"><img src="<?= base_url('assets/img/snoopy_pasto.png');?>" width="100%"></div>
            </div>

         </div>
      </div>
      <div class="col-md-5 columns">
         <div class="casaSnoopy col-xs-12"><img src="<?= base_url('assets/img/casita.png');?>" width="100%"></div>
      </div>
   </div>

   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?= base_url('assets/img/close.png');?>"></button>
            </div>
            <div class="modal-body text-center">
               <a href="<?= $authUrl ?>" class="participacion" data-analytics="Login Facebook"><img src="<?= base_url('assets/img/facebook.png');?>"></a>
            </div>
         </div>
      </div>
   </div>




