<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Snoopy_model extends CI_Model{
    function __construct() {
        parent::__construct();
    }
    /**** GENERICAS */
    public function onlyInsert($tabla, $data){
        $this->romina->timeStamp($tabla);
        $this->db->insert($tabla, $data);
        $id=$this->db->insert_id();
        return $id ? $id : FALSE;
    }

    public function onlyGet($tabla, $where){
        $query = $this->db->get_where($tabla,$where);
        return($query->num_rows() != 0 ? $query->result() : FALSE);
    }

    public function onlyUpdate($tabla, $data, $dataWhere){
        $this->romina->timeStamp($tabla, FALSE);
        $this->db->where($dataWhere);
        return $this->db->update($tabla, $data);
    }
}
