$(document).ready(function(){
// DECLARACIÓN DE LA FUNCION

function toggleNavigation(){
	$('.page-header').toggleClass('menu-expanded');
	$('.page-nav').toggleClass('collapse');
}

	// EVENTOS DEL DOM
	$(window).on('load',function(){
		$('.toggle-nav').click(toggleNavigation);
	});


	$('#btn-light').on('click', '[data-toggle="lightbox"]', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});


	$('#btnZoom').on('click',function(event){
		$(".imagen360").removeClass("nozoom");
		$(".imagen360").addClass("zoom");
	});

	$('#btnnoZoom').on('click',function(event){
		$(".imagen360").removeClass("zoom");
		$(".imagen360").addClass("nozoom");
	});


	$('#btnZoomMov').on('click',function(event){
		$(".reelMov").removeClass("nozoom");
		$(".reelMov").addClass("zoom");
	});

	$('#btnnoZoomMov').on('click',function(event){
		$(".reelMov").removeClass("zoom");
		$(".reelMov").addClass("nozoom");
	});

	$(".participacion").on('click',function(e){
		ga('send', {
			hitType: 'event',
			eventCategory: 'Participación',
			eventAction: 'click',
			eventLabel: $(this).attr('data-analytics')
		});
	});

	$('.aceptarprivacidad').on('click',function(){
		Cookies.set('cookietime','1');
		$('.privacidad').fadeOut('slow');
	});
	$('.privacidad').delay(8000).fadeOut('slow');
// $('.aceptarprivacidad').on('click',function(e){
// 	$('.privacidad').hide();
// });
	if(typeof Cookies.get('cookietime') === 'undefined'){
		console.log('nohaycookie');
		$('.privacidad').show();
	} else {
		$('.privacidad').hide();
	}

});
